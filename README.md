# SLUD-XIX-Material-Grafico

En este repositorio se manejaran los editables y entregables de los materiales graficos para el SLUD XIX.


## Entregables

Se ubican los archivos que estan listos para publicar o hacer uso se ellos.

## Editables

Se ubican los archivos editables de diseño.

## Referencias_diseño

Se ubican la presentación de las bases para los diseños: Paleta, logo, elementos, tipografia...

La idea es mantener una uniformidad de todos los entregables. Es posible modificar las bases sin ningun problema, si se desean generalizar, los editables de las bases se encuentran en ***Editables***.

### Drive

Para archivos demasiados grandes se recomienda subirlos al Drive: https://drive.google.com/drive/u/0/folders/1lrmWNhnUoqOKgJnx8gTXvCXu2vqBlGv7 
